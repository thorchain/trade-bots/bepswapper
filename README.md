# Pool Balancer designed by Credence capital

# Objective: 

* This is a sample swapping bot for use on Thorchain. It comes with the following attributes

* Keep BEPSwap test-net pool prices correct against their Binance Dex equivalent. Eg. Keep $RUNE price on BEPSwap.com as close to the $RUNE price on www.binance.org

# In Scope: 

* Swapping across BEPSwap pools with test-net assets to change pool balances such that the pool price of an asset reflects it's binance dex mainnet equivalent.

# Out of scope:  

* No actual external market actions required for this as the balancing is done within BEPSwap.com. 

* No buying/selling on binance dex or any other market required.

# High Level Steps:

* Calculate main-net asset market price (by binance API lastPrice, weightedAvgPrice or by order book depths)

* Calculate BEPSwap pool price for the asset (using mid-gard API endpoints to get pool balances)

* Calculate the delta between these two prices ie. price delta / arbitrage opportunity.

* Calculate the trade size required on BEPSwap to restore the pool to the correct market price.

* Get the THORChain pool address from mid-gard endpoint.

* Send the assets calculated in step #4 and wait for the pool balances to update. Start again.

# How to run the swapper

source venv/bin/activate
pip install -r requirements.txt
streamlit run Balancer.py

