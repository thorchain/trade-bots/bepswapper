import concurrent.futures
import sys
import time

from datetime import datetime, timezone
from pprint import pprint

from engine.thorchain import Thorchain
from engine.binancedex import BinanceDex
from engine.repository import Repository

class Source:
    Dex = 'dex'
    Thor = 'thor'

class Updater:

    def __init__(self, config: dict, repository: Repository):
        self._config = config
        self._repo = repository

    def _grider(self, fn, key:tuple, grid: dict):
        product, source = key
        symbol = self._config['products'][product][source]
        if symbol:
            grid[key] = fn(symbol)

    def Run(self):

        t0 = time.time()
        now = datetime.now(timezone.utc).replace(microsecond=0).isoformat()
        grid = dict()

        with concurrent.futures.ThreadPoolExecutor() as executor:
            for asset, keys in self._config['products'].items():
                executor.submit(self._grider, BinanceDex(self._config).GetLatest, (asset, Source.Dex), grid)
                executor.submit(self._grider, Thorchain(self._config).GetLatest, (asset, Source.Thor), grid)

        for key, value in grid.items():
            asset, source = key
            if source == Source.Dex:
                self._repo.SetSummaryDex(asset, value)
            elif source == Source.Thor:
                self._repo.SetSummaryThor(asset, value)

        update_time = time.time() - t0
        # self._repo.SetSummaryTime(now)
        # self._repo.SetUpdateTime(update_time)

        sys.stdout.write(f'{now} :: {len(grid)} products loaded in {update_time:0.1f}s\n')
        sys.stdout.flush()

