import time
import requests

def get_best_node(config):
    assert isinstance(config['thorchain']['pools'], list)
    assert len(config['thorchain']['pools']) > 0
    return sorted(config['thorchain']['pools'], key=lambda x: x['delay'])[0]

def _load_reserve(config):
    print(f"Load reserve from {config['thorchain']['reserve_node']}")

    config['thorchain']['pools'] = []
    r = requests.get(f"http://{config['thorchain']['reserve_node']}{config['thorchain']['address_template']}", timeout=2)
    address = f"http://{config['thorchain']['reserve_node']}:{config['thorchain']['port']}"

    config['thorchain']['pools'].append(dict(address=address,
                                             wallet = r.json()['BNB'][0],
                                             delay=0,
                                             assets=requests.get(address + '/v1/pools').json()
                                             ))
    return config

def _load_thorchain(config):
    if 'reserve_node' in config['thorchain'].keys():
        return _load_reserve(config)

    print('Loading', end='')
    point = config['thorchain']['network']

    config['thorchain']['pools'] = []

    for pool in requests.get(point, timeout=2).json():
        try:
            t0 = time.time()
            r = requests.get(f"http://{pool}{config['thorchain']['address_template']}", timeout=2)
            address = f"http://{pool}:{config['thorchain']['port']}"
            config['thorchain']['pools'].append(dict(
                address = address,
                wallet=r.json()['chains']['BNB'][0],
                delay=time.time() - t0,
                assets=requests.get(address + '/v1/pools').json()
            ))
            print('.', end='')
        except Exception as e:
            print(e)

    print()
    return config

def handle_config(config):
    config = _load_thorchain(config)
    return config

def load_style(st, filename):
    with open(filename, 'r') as fp:
        st.markdown(f'<style>{fp.read()}</style>', unsafe_allow_html=True)

def show_title(st, title):
    st.markdown(f'<div class="stHeader">{title}</div>', unsafe_allow_html=True)