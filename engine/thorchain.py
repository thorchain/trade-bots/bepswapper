from http import HTTPStatus

import requests

from engine.helpers import get_best_node

class Thorchain:

    def __init__(self, config):
        self._config = config
        self._url = get_best_node(config)['address']

    def _api_request(self, endpoint, params=None):
        r = requests.get(self._url + endpoint, params=params or dict())
        return r.json() if r.status_code == HTTPStatus.OK else None

    def _get_pools_data(self, asset):
        data = self._api_request('/v1/pools/detail', dict(asset=asset))
        return data

    def _get_asset_pools(self):
        return self._api_request('/v1/poools')

    def _get_stakers(self):
        return self._api_request('/v1/stakers')

    def GetAssets(self):
        return self._get_asset_pools()

    def GetLatest(self, symbol):
        data = self._api_request('/v1/pools/detail', dict(asset=symbol))
        if isinstance(data, list):
            return data[0]

    def GetLatestPrice(self, symbol):
        data = self.GetLatest(symbol)
        if isinstance(data, dict):
            return float(data.get('price', None))
