import time
from datetime import datetime, timezone

import streamlit as st
import pandas as pd

from engine.calculator import Calculator
from view.view_community import ViewCommunity

class ViewSummary(ViewCommunity):

    def __init__(self, config: dict, placeholders: dict, calculator: Calculator):
        super().__init__(config, placeholders, calculator)

    def Run(self, title):
        super().Run(title)

        while True:
            self._calculator.Update()

            now = datetime.now(tz=timezone.utc).replace(microsecond=0)
            self._placeholders['time'].markdown(f'<center><b>{now.isoformat()}</b></center', unsafe_allow_html=True)

            df = pd.DataFrame()
            columns = []

            for product in [x for x in self._config['products'] if x != 'RUNE']:
                data = self._calculator.GetSummary(product)

                row = {
                    'Product': data['product'],
                    'Asset Depth': data['assetDepth'],
                    'Rune Depth': data['runeDepth'],
                    'Pool Price (USD)': data['pool_in_usd'],
                    'Delta Market/Pool (%)': data['delta_pct'],
                }
                df = df.append(row, ignore_index=True)
                columns = [x for x in row.keys() if x != 'Product']
            df.set_index('Product', inplace=True)
            self._placeholders['data'].table(df[columns])
            time.sleep(1)
