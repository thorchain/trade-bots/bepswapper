import streamlit as st
import pandas as pd

from engine.calculator import Calculator
from view.view_community import ViewCommunity

class ViewProduct(ViewCommunity):

    def __init__(self, config: dict, placeholders: dict, calculator: Calculator, product: str):
        super().__init__(config, placeholders, calculator)
        self._product =  product

    def _get_product_data(self, product, data):
        return [x for x in data if x['product'] == product][0]

    def Run(self, title):
        super().Run(title)

        self._calculator.Update()
